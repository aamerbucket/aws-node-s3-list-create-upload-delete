# Steps
1. Configure aws-sdk
1. For creating a bucket - hit ``` node .\create.js taj-mahal-by-aamer ```
1. For listing all buckets - hit ```` node .\list.js ```
1. For Uploading - hit ``` node .\upload.js taj-mahal-by-aamer .\taj-mahal.txt ```
    > NOTE: Here, the taj-mahal.txt is created and uploaded
1. For listing all the objects of a specific bucket - hit ``` node .\listing-bucket-objects.js  ```
    > NOTE: Make sure you should add the bucket name and the region where the bucket is deployed
1. For Deleting - Bucket must be empty - hit ``` node delete.js ```